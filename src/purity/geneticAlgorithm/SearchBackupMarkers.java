package purity.geneticAlgorithm;

import io.jenetics.EnumGene;
import io.jenetics.Optimize;
import io.jenetics.PartiallyMatchedCrossover;
import io.jenetics.PermutationChromosome;
import io.jenetics.Phenotype;
import io.jenetics.RouletteWheelSelector;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionStatistics;
import io.jenetics.engine.Limits;
import io.jenetics.util.IntRange;

import static io.jenetics.engine.EvolutionResult.toBestPhenotype;

import java.io.IOException;
import java.util.ArrayList;

import net.maizegenetics.dna.map.PositionList;
import net.maizegenetics.dna.snp.FilterGenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;

public class SearchBackupMarkers {

    public static void main(String[] argv) throws IOException{
        int selectNumberOfMarkers = 10;
        int populationSize = 100;
        int startPos = 0;
        double maxDistance = 0.0;
        String filename = "C:/Users/jignacio/Google Drive/Trainings/R/purity/skim_and_3k_elite_core.hmp.txt";
        String output = "C:/Users/jignacio/Google Drive/Trainings/R/purity/skim_and_3k_elite_core.hmp.txt.purity.out";
        String fileToBackup = "C:/Users/jignacio/Google Drive/Trainings/R/purity/skim_and_3k_elite_core.hmp.txt";
        GenotypeTable genoToBackup;
        GenotypeTable genos;
        
        if (argv.length == 6 || argv.length == 7) {
            try {
                selectNumberOfMarkers = Integer.parseInt(argv[0]);
                populationSize = Integer.parseInt(argv[1]);
                maxDistance = Double.parseDouble(argv[2]);
                filename = argv[3];
                output = argv[4];
                fileToBackup = argv[5];
            } catch(NumberFormatException e) {
              System.err.println("Arguments invalid (" + e.getMessage() + ").");
              System.exit(1);
            }
        }
        else {
            System.err.println("usage: <Number of target markers> <Population size> <Distance bet. dup lines> <input hmp.txt> <output file> <file to backup> <opt: start pos>");
            System.exit(1);
        }
        
        if (argv.length == 7) 
            startPos = Integer.parseInt(argv[6]);
        
        PassParameters.setMaxDistance(maxDistance);
        PassParameters.setFilename(filename);
        genos = ImportUtils.readFromHapmap(filename);
        PassParameters.setGenos(genos);
        genoToBackup = ImportUtils.readFromHapmap(fileToBackup);
        ArrayList<String> siteNamesToRemoveList = new ArrayList<String>();
        for(int i = 0; i < genoToBackup.numberOfSites()-startPos; i++){
            siteNamesToRemoveList.add(genoToBackup.siteName(i+startPos));
        }
        genoToBackup = FilterGenotypeTable.getInstanceRemoveSiteNames(genoToBackup, siteNamesToRemoveList);
        PassParameters.setGenoToBackup(genoToBackup);
        PassParameters.setOutput(output);
        
        // 3.) Create the execution environment.
        final Engine<EnumGene<Integer>, Integer> engine = Engine
            .builder(GeneticAlgorithm::getUniquesWithBackup, PermutationChromosome.ofInteger(IntRange.of(0,GeneticAlgorithm.getTotalNumberOfMarkers()-1), selectNumberOfMarkers))
            .optimize(Optimize.MAXIMUM) 
            .maximalPhenotypeAge(7) 
            .populationSize(populationSize)
            .offspringFraction(0.7)
            .survivorsSelector(new RouletteWheelSelector<>())
//            .offspringSelector(new RouletteWheelSelector<>())
            .alterers( 
//                new Mutator<>(0.01), 
                new PartiallyMatchedCrossover<>(0.35)
                ) 
            .build();
 
        // 4.) Start the execution (evolution) and
        //     collect the result.  
        final EvolutionStatistics<Integer, ?> 
        statistics = EvolutionStatistics.ofNumber(); 
        
        final Phenotype<EnumGene<Integer>, Integer> result = engine.stream()
            //.limit(limit.byExecutionTime(Duration.ofMinutes(10)))
            .limit(Limits.bySteadyFitness(11))
            //.limit(limit.byFitnessThreshold(1651))
            .limit(100)
            .peek(GeneticAlgorithm::update)
            .peek(statistics)
            .collect(toBestPhenotype());
        
        
//        Path file = Paths.get(output+".pur.scr");
//        File markerOutFile = new File(output+".pur.snp");

//        File markerOutFile = new File(output);
//        PositionList pl = GeneticAlgorithm.getBestMarkerInfo(result);

//        File purityScore = new File(output+".pur.scr");
//
//        if(purityScore.exists()){
//            try {
//                Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
//            }catch (IOException e) {
//                System.err.println(e.getMessage());
//            }
//        }else{
//                Files.write(file, lines, Charset.forName("UTF-8"));
//        }
//        
//        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(pl),"\t",markerOutFile); 
        
        System.out.println(statistics);
        System.out.println("Result:\n" + result);
        System.out.println("Results written to:");
        System.out.println(output);
    }

}
