package purity.geneticAlgorithm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import net.maizegenetics.dna.snp.GenotypeTable;

public class PairwisePolymorphism {
    private int[][] numPolySNPsMatrix;
    private String[] taxaNames;
    
    public PairwisePolymorphism(GenotypeTable geno){
        Build(geno);
    }
    
    private void Build(GenotypeTable geno){
        int size = geno.numberOfTaxa();
        numPolySNPsMatrix = new int[size][size];
        taxaNames = new String[size];
        for(int i=0; i < geno.numberOfTaxa(); i++){
            taxaNames[i] = geno.taxaName(i);
            for(int j=i+1; j<geno.numberOfTaxa(); j++){
                numPolySNPsMatrix[i][j] = computePairwisePolymorphism(geno.genotypeAllSites(i),geno.genotypeAllSites(j),geno.genotypeAsStringRow(i),geno.genotypeAsStringRow(j));
                numPolySNPsMatrix[j][i] = numPolySNPsMatrix[i][j];
            }
        }
    }
    
    private int computePairwisePolymorphism(byte[] i, byte[] j, String s, String r){
        int numPolySNPs = 0;
        for(int k=0; k<i.length; k++){
            if(i[k] != -1 || j[k] != -1){ // no missing
                if(i[k] % 17 == 0 && j[k] % 17 == 0){ // consider only if both genotypes are homozygous
                    if(i[k] != j[k]) // if polymorphic
                        numPolySNPs++;
                }
            }
        }
        return numPolySNPs;
    }
    
    public int getNumPolySNPs(int i, int j){
        return numPolySNPsMatrix[i][j];
    }
    
    public int[][] getNumPolySNPsMatrix(){
        return numPolySNPsMatrix;
    }
    
    public int countLinesWithNumPolySNPs(int x){
        int nlines = 0;
        for(int i = 0; i < taxaNames.length; i++) {
            for(int j = 0; j < i; j++) {
                if(numPolySNPsMatrix[i][j] >= x)
                    nlines++;
            }
        }
        return nlines;
    }

    public int countLinesWithNumPolySNPs(){
        int nlines = 0;
        for(int i = 0; i < taxaNames.length; i++) {
            for(int j = 0; j < i; j++) {
                if(numPolySNPsMatrix[i][j] > 0)
                    nlines++;
            }
        }
        return nlines;
    }
    
    public int remainingLinesWithoutPolySNPs(int x){
        int nlines = 0;
        int totalLines = taxaNames.length;
        int nPairwiseCombinations = ((totalLines*totalLines)-totalLines)/2;
        for(int i = 0; i < taxaNames.length; i++) {
            for(int j = 0; j < i; j++) {
                if(numPolySNPsMatrix[i][j] >= x)
                    nlines++;
            }
        }
        return nPairwiseCombinations - nlines;
    }
        
    public void printNumPolySNPsMatrix(String filename){
        StringBuilder strBld = new StringBuilder();
        strBld.append("numPairwisePolySNPs"+",");
        for(int t = 0; t < taxaNames.length; t++){
            strBld.append(taxaNames[t]);
            if(t < taxaNames.length - 1)
                strBld.append(",");
        }
        strBld.append("\n");
        for(int i = 0; i < numPolySNPsMatrix.length; i++){
            strBld.append(taxaNames[i]+",");
            for (int j = 0; j < numPolySNPsMatrix.length; j++){
                strBld.append(numPolySNPsMatrix[i][j]+"");
                if(j < numPolySNPsMatrix.length - 1)
                    strBld.append(",");
            }
            strBld.append("\n");
        }
        
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
            writer.write(strBld.toString());
            writer.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
