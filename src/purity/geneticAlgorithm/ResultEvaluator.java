package purity.geneticAlgorithm;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.maizegenetics.dna.map.PositionListTableReport;
import net.maizegenetics.dna.snp.ExportUtils;
import net.maizegenetics.dna.snp.FilterGenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.TaxaListBuilder;
import net.maizegenetics.util.TableReportUtils;

public class ResultEvaluator {
    private static final int[] result = {32821,9135,10017,19320,35516,18095,35438,9811,6432,19237,34246,5759,28668,8628,15120,5286,17655,30620,20247,4637,16773,34039,2790,24385};
    private static final int[] result2 = {8993,27262,1197,11100,18095,35516,29315,3407,4227,6155,8568,7798,32732,15849,34246,4592,1217,16041,26462,16943,33225,29354,11461,35438};
    private static final int[] result3 = {7208,31693,35272,21688,18095,35516,12658,25802,4227,19320,24149,4606,30513,11014,1197,34246,22133,16041,18123,16943,35438,5205,33897,23169};
    private static final int[] result4 = {7208,26296,2369,13505,19320,35516,32732,25802,4227,18095,22193,34917,20725,11079,34246,4629,3033,16041,18123,35168,16943,3891,3407,35438};
    private static final int[] result5 = {8993,22933,2369,24626,18095,35516,3407,25802,22193,5205,20069,19650,8353,32806,16447,34246,35126,16041,27658,21850,16943,29354,11461,16632};
    private static final int[] result6 = {7208,22933,19541,26482,17585,35516,32732,3407,4227,18095,20069,34917,20725,8568,34246,4629,33070,16041,22129,20141,16943,5205,12658,35438};
    private static final int[] result7 = {25295,25397,15470,15742,23840,7501,27361,14225,4452,10654,6902,11409,21479,13593,16251,17303,26816,1777,11879,12945,22166,26196,14875,5202};
    private static final int[] result8 = {4,1,8,13,12,11,2,3,10,9};
//    private static final int[] result = {14411,34116,12379,19212,4007,8716,2790,30110,30214,21297};
//    private static final int[] result2 = {11096,36321,8716,12379,32945,29982,29649,34246,30214,7310};
//    private static final int[] result3 = {4024,12379,31118,30214,34176,5735,1824,7807,20123,21972};
//    private static final int[] result4 = {35925,24269,4038,20457,30214,20948,6953,8806,17057,7286};
//    private static final int[] result5 = {30214,9091,4024,34153,7707,6006,20457,17324,19545,18029};

    public static void main(String[] args){
        String input = "C:/Users/jignacio/Downloads/purity_171218/test.hmp.txt"; 
        String input3 = "/home/carli/workspace/osativa-kasp-analysis/subpop-analysis/accession_level_genotyping_sorted.hmp.txt";
        String outFile = "C:\\Users\\jignacio\\Downloads\\SelectDiversity_307S_1536M.hmp.out.txt";
       
        String polySNPsFile = "C:\\Users\\jignacio\\Downloads\\SelectDiversity_307S_1536M.hmp.out.numPairwisePoly.csv";
//        String input2 = "C:/Users/jignacio/Downloads/ind_core_ld_pruneexcluded_near_straits.hmp.txt.gz";
//        
//        GenotypeTable geno = ImportUtils.readFromHapmap(input);
        GenotypeTable geno3 = ImportUtils.readFromHapmap(input3);
        
//        GenotypeDuplicity gd = new GenotypeDuplicity(geno3);
//        GenotypeDuplicity gd3 = new GenotypeDuplicity(geno3,0.01);
//        GenotypeDuplicity gd4 = new GenotypeDuplicity(geno3,0.05);
        String[] siteNamesToKeep = new String[24];
        ArrayList<String> siteNamesToKeepList = new ArrayList<String>();
        
//        Arrays.sort(result8);
//        
//        for (int i = 0; i < result8.length; i++){
//            //siteNamesToKeep[i] = geno.siteName(result5[i]);
//            //siteNamesToKeepList.add(String.format("chr%02d", Integer.parseInt(geno.chromosomeName(result5[i])))+":"+geno.chromosomalPosition(result5[i]));
//            siteNamesToKeepList.add(geno3.siteName(result8[i]));
//            System.out.println(siteNamesToKeepList.get(i));
//        }
//        
//        geno3 = FilterGenotypeTable.getInstance(geno3, siteNamesToKeepList);
        
//        File markerOutFile = new File(outFile+"_markerinfo.out");
//        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(geno3.positions()),"\t",markerOutFile);
//        GenotypeTable geno2 = ImportUtils.readFromHapmap(input2);
        

        GenotypeDuplicity gd2 = new GenotypeDuplicity(geno3,0.0);
//        GenotypeDuplicity gd2 = gd3;

//        GenotypeDuplicity gd3 = new GenotypeDuplicity(FilterGenotypeTable.getInstance(geno,result5));
//        GenotypeDuplicity gd4 = new GenotypeDuplicity(FilterGenotypeTable.getInstance(geno2,result5));
//        
//        gd.printFullDuplicityInfo();
//        gd3.printFullDuplicityInfo();
//        
          gd2.printFullDuplicityInfo();
          for(int i = 0; i < gd2.getDuplicatedTaxa().size(); i++){
              if(gd2.getDuplicatedTaxa().get(i).size() > 1){
                  for (int j = 0; j < gd2.getDuplicatedTaxa().get(i).size(); j++)                  
                      System.out.print(geno3.taxaName(gd2.getDuplicatedTaxa().get(i).get(j))+"\t");
                  System.out.print("\n");
              }
          }
          
          PairwisePolymorphism pp1 = new PairwisePolymorphism(geno3);
          pp1.printNumPolySNPsMatrix(polySNPsFile);
          
          System.out.println(gd2.getTaxaSubgroup());
          
          TaxaList taxa = geno3.taxa();
          TaxaListBuilder tlb = new TaxaListBuilder();
          
          for(int i = 0; i < gd2.getDuplicatedTaxa().size(); i++){
              if(gd2.getDuplicatedTaxa().get(i).size() > 1){
                  int indexOfTaxaToKeep = gd2.getDuplicatedTaxa().get(i).get(0);
                  for (int j = 1; j < gd2.getDuplicatedTaxa().get(i).size(); j++){
                      int indexOfCurrentTaxa = gd2.getDuplicatedTaxa().get(i).get(j);
                      if(geno3.totalNonMissingForTaxon(indexOfTaxaToKeep) < geno3.totalNonMissingForTaxon(indexOfCurrentTaxa)){
                          indexOfTaxaToKeep = indexOfCurrentTaxa;
                      }
                  }
                  tlb.add(taxa.get(indexOfTaxaToKeep));
              }else{
                  tlb.add(taxa.get(gd2.getDuplicatedTaxa().get(i).get(0)));
              }
          }
          
          taxa = tlb.build();
          
//          geno3 = FilterGenotypeTable.getInstance(geno3, taxa);
//          ExportUtils.writeToHapmap(geno3, outFile+".out.hmp.txt");
//        gd4.printFullDuplicityInfo();
//
//        GenotypeTable geno3 = ImportUtils.readFromHapmap(input3);
//        GenotypeDuplicity gd5 = new GenotypeDuplicity(geno3);
//        GenotypeDuplicity gd6 = new GenotypeDuplicity(FilterGenotypeTable.getInstance(geno3,result5));
//        gd5.printFullDuplicityInfo();
//        gd6.printFullDuplicityInfo();
        
    }
}
