package purity.geneticAlgorithm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class LDMatrix {
    float[][] matrix = null;
    
    public LDMatrix(String filename) throws IOException {
        String myFilename = filename;
        
        // If included in an Eclipse project.
        InputStream stream = ClassLoader.getSystemResourceAsStream(myFilename);
        BufferedReader buffer = new BufferedReader(new InputStreamReader(stream));

        // If in the same directory - Probably in your case...
        // Just comment out the 2 lines above this and uncomment the line
        // that follows.
        //BufferedReader buffer = new BufferedReader(new FileReader(filename));

        String line;
        int row = 0;
        int size = 0;

        while ((line = buffer.readLine()) != null) {
            String[] vals = line.trim().split("\\t");

            // Lazy instantiation.
            if (matrix == null) {
                size = vals.length;
                matrix = new float[size][size];
            }

            for (int col = 0; col < size; col++) {
                matrix[row][col] = Integer.parseInt(vals[col]);
            }

            row++;
        }
    }
    
    public float[][] getMatrix() {
        return matrix;
    }
}
