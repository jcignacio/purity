package purity.geneticAlgorithm;

import java.io.File;

import net.maizegenetics.dna.map.PositionListTableReport;
import net.maizegenetics.dna.snp.ExportUtils;
import net.maizegenetics.dna.snp.FilterGenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.util.TableReportUtils;

public class ReportPosition {
    private static final int[] result = {14411,34116,12379,19212,4007,8716,2790,30110,30214,21297};
    private static final int[] result2 = {11096,36321,8716,12379,32945,29982,29649,34246,30214,7310};
    private static final int[] result3 = {4024,12379,31118,30214,34176,5735,1824,7807,20123,21972};
    private static final int[] result4 = {35925,24269,4038,20457,30214,20948,6953,8806,17057,7286};
    private static final int[] result5 = {30214,9091,4024,34153,7707,6006,20457,17324,19545,18029};
    
    public static void main(String[] args){
        String input = "C:/Users/jignacio/Downloads/ind_core_ld_prune_excluded_near_straits_nohets_0.05_dist.hmp.txt";
        //String outFile = "C:/Users/jignacio/Google Drive/GSL/skimseq/elite_3k_core/all_filt_ld_purity_10_JI001";
        
        GenotypeTable geno = ImportUtils.readFromHapmap(input);
        GenotypeTable geno1 = FilterGenotypeTable.getInstance(geno, result);
        GenotypeTable geno2 = FilterGenotypeTable.getInstance(geno, result2);
        GenotypeTable geno3 = FilterGenotypeTable.getInstance(geno, result3);
        GenotypeTable geno4 = FilterGenotypeTable.getInstance(geno, result4);
        GenotypeTable geno5 = FilterGenotypeTable.getInstance(geno, result5);
        File markerOutFile = new File("C:/Users/jignacio/Google Drive/GSL/skimseq/elite_3k_core/all_filt_ld_purity_10_JI001.txt");
        File markerOutFile2 = new File("C:/Users/jignacio/Google Drive/GSL/skimseq/elite_3k_core/all_filt_ld_purity_10_JI002.txt");
        File markerOutFile3 = new File("C:/Users/jignacio/Google Drive/GSL/skimseq/elite_3k_core/all_filt_ld_purity_10_JI003.txt");
        File markerOutFile4 = new File("C:/Users/jignacio/Google Drive/GSL/skimseq/elite_3k_core/all_filt_ld_purity_10_JI004.txt");
        File markerOutFile5 = new File("C:/Users/jignacio/Google Drive/GSL/skimseq/elite_3k_core/all_filt_ld_purity_10_JI005.txt");
        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(geno1.positions()),"\t",markerOutFile);
        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(geno2.positions()),"\t",markerOutFile2);
        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(geno3.positions()),"\t",markerOutFile3);
        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(geno4.positions()),"\t",markerOutFile4);
        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(geno5.positions()),"\t",markerOutFile5);
        
        ExportUtils.writeToHapmap(geno5, "C:/Users/jignacio/Google Drive/GSL/skimseq/elite_3k_core/all_filt_ld_purity_10_JI005.hmp.txt");
    }
}
