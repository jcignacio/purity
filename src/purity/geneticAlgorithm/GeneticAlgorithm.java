package purity.geneticAlgorithm;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import io.jenetics.EnumGene;
import io.jenetics.Genotype;
import io.jenetics.Phenotype;
import io.jenetics.engine.EvolutionResult;
import net.maizegenetics.analysis.distance.IBSDistanceMatrix;
import net.maizegenetics.dna.snp.CombineGenotypeTable;
import net.maizegenetics.dna.snp.ExportUtils;
import net.maizegenetics.dna.snp.FilterGenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.map.PositionListTableReport;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.TaxaListBuilder;
import net.maizegenetics.taxa.TaxaListTableReport;
import net.maizegenetics.taxa.distance.DistanceMatrix;
import net.maizegenetics.util.TableReportUtils;
import purity.geneticAlgorithm.GenotypeDuplicity;
import purity.geneticAlgorithm.PairwisePolymorphism;

public class GeneticAlgorithm {
    // 2.) Definition of the fitness function.
    //private static final String filename = "C:/Users/jignacio/Google Drive/Trainings/R/purity/input.m24.row5.out.hmp.LD.matrix.txt";
//    private static final String filename = "D:/all_filt_hd.txt.LD.matrix.replaced.txt";
//    private static final float[][] LDvalues = matrix(filename);
//    private static String dir="C:/Users/jignacio/Google Drive/Trainings/R/purity/";
//    private static String inFile = PassParameters.getFilename();
    public static double maxDistance = PassParameters.getMaxDistance();
    public static int nPoly = PassParameters.getNPoly();
    public static int numberOfControlTaxa = PassParameters.getNumberOfControlTaxa();
    private static GenotypeTable genos = PassParameters.getGenos();
    private static GenotypeTable genoToBackup = PassParameters.getGenoToBackup();
    //private static GenotypeTable genos_1 = GenotypeTableBuilder.getHomozygousInstance(ImportUtils.readFromHapmap(inFile));
    //private static GenotypeTable genos = FilterGenotypes.removeDuplicateTaxa(genos_1, maxDistance);
    public static int totalNumberOfMarkers = genos.numberOfSites();
    public static int totalNumberOfTaxa = genos.numberOfTaxa()-numberOfControlTaxa;
    static Phenotype<EnumGene<Integer>, Integer> best = null;
    public static SampleMetadata sampleMetadata = PassParameters.getSampleMetadata();
    
    public static int getTotalNumberOfMarkers(){
        return totalNumberOfMarkers;
    }
    
    public static int getTotalNumberOfTaxa(){
        return totalNumberOfTaxa;
    }
    
    //private static final File myFile = new File(filename); 
//    private static float[][] matrix(String filename)  {
//        float[][] matrix = null;
//                //String filename = "C:/Users/jignacio/Google Drive/Trainings/R/purity/input.m24.row5.out.hmp.LD.matrix.txt";
//                
//        // If included in an Eclipse project.
//        BufferedReader buffer = null;
//        try {
//            buffer = new BufferedReader(new FileReader(filename));
//        } catch (FileNotFoundException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    
//        String line;
//        int row = 0;
//        int size = 0;
//
//        try {
//            while ((line = buffer.readLine()) != null) {
//                String[] vals = line.trim().split("\\t");
//
//                // Lazy instantiation.
//                if (matrix == null) {
//                    size = vals.length;
//                    matrix = new float[size][size];
////                    for (int col = 0; col <= size; col++)
////                        System.out.print(col+"\t");
////                    System.out.print("\n");
//                }
//
////                System.out.print(row+1+"\t");
//                
//                for (int col = 0; col < size; col++) {
//                    matrix[row][col] = Float.parseFloat(vals[col]);
////                    System.out.print(matrix[row][col]+"\t");
//                }
////                System.out.print("\n");
//                row++;
//            }
//        } catch (NumberFormatException | IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        return matrix;
//    }
    
//    private static double getSumVerbose(Genotype<EnumGene<Integer>> gene) {
//         int[] markers = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
//         double gsum = IntStream.range(0,markers.length).mapToDouble(i -> markers[i]).sum();
//         for (int i = 0; i < markers.length; i++){
//           //  System.out.print(markers[i]+"|");
//         }
//        // System.out.println("\tSum: "+gsum);
//         return gsum;
//    }
//    
//    private static double getSumInts(Genotype<EnumGene<Integer>> gene) {
//        return gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).sum();
//    }
//    
//    private static double getSum(Genotype<EnumGene<Integer>> gene) {
//        int[] markers = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
//        double gsum = 0;
//        for (int i = 0; i < markers.length; i++){
//            for (int j = i+1; j < markers.length; j++){
//                double value = LDvalues[markers[i]][markers[j]];
//                //System.out.print(value+"|");
//                gsum=gsum+value;
//            }
//        }
//        for (int i = 0; i < markers.length; i++){
//            //System.out.print(markers[i]+"|");
//        }
//        //System.out.print("\t"+gsum+"\n");
//        return gsum;
//    }
    
    
    public static int nPolymorphic(Genotype<EnumGene<Integer>> gene) {
        ArrayList<String> keepSites= new ArrayList<>();
        int[] markers = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
        for (int site = 0; site < markers.length; site++) {
            keepSites.add(genos.siteName(markers[site]));
        }
            
        GenotypeTable filterKeep= FilterGenotypeTable.getInstance(genos, keepSites.toArray(new String[keepSites.size()]));

        PairwisePolymorphism pp = new PairwisePolymorphism(filterKeep);
        
        return pp.countLinesWithNumPolySNPs(nPoly);
    }
    
    public static int nNotPolymorphic(Genotype<EnumGene<Integer>> gene) {
        ArrayList<String> keepSites= new ArrayList<>();
        int[] markers = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
        for (int site = 0; site < markers.length; site++) {
            keepSites.add(genos.siteName(markers[site]));
        }
            
        GenotypeTable filterKeep= FilterGenotypeTable.getInstance(genos, keepSites.toArray(new String[keepSites.size()]));

        PairwisePolymorphism pp = new PairwisePolymorphism(filterKeep);
        
        return pp.remainingLinesWithoutPolySNPs(nPoly);
    }
    
    
    public static int getUniques(Genotype<EnumGene<Integer>> gene) {
        ArrayList<String> keepSites= new ArrayList<>();
        int[] markers = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
        for (int site = 0; site < markers.length; site++) {
            keepSites.add(genos.siteName(markers[site]));
        }
            
        //System.out.println("Out of "+genos.numberOfSites()+" keep "+keepSites.size()+" sites");
        GenotypeTable filterKeep= FilterGenotypeTable.getInstance(genos, keepSites.toArray(new String[keepSites.size()]));
        //ProgressListener listener = new ProgressListener; 

        DistanceMatrix distMatrix = IBSDistanceMatrix.getInstance(filterKeep);
        GenotypeDuplicity dupes = new GenotypeDuplicity(distMatrix, maxDistance);
        //System.out.print(dupes.getUniqueTaxa()+"\t");
        //System.out.println(Arrays.toString(markers));
        return dupes.getUniqueTaxa();
        //return dupes.getTaxaSubgroup();
    }
    
    public static int getUniquesWithBackup(Genotype<EnumGene<Integer>> gene) {
        ArrayList<String> keepSites= new ArrayList<>();
        int[] markers = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
        for (int site = 0; site < markers.length; site++) {
            keepSites.add(genos.siteName(markers[site]));
        }
            
        GenotypeTable filterKeep= FilterGenotypeTable.getInstance(genos, keepSites.toArray(new String[keepSites.size()]));
        
        GenotypeTable[] tables = {filterKeep,genoToBackup};
        GenotypeTable mergedGeno= CombineGenotypeTable.getInstance(tables);
        
        DistanceMatrix distMatrix = IBSDistanceMatrix.getInstance(mergedGeno);
        GenotypeDuplicity dupes = new GenotypeDuplicity(distMatrix, maxDistance);
        return dupes.getUniqueTaxa();
        //return dupes.getTaxaSubgroup();
    }    
    
    public static int getDuplicateGenosBetweenTraitGroups(Genotype<EnumGene<Integer>> gene) {
        ArrayList<String> keepSites= new ArrayList<>();
        int[] markers = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
        for (int site = 0; site < markers.length; site++) {
            keepSites.add(genos.siteName(markers[site]));
        }
            
        GenotypeTable filterKeep= FilterGenotypeTable.getInstance(genos, keepSites.toArray(new String[keepSites.size()]));
        
        DistanceMatrix distMatrix = IBSDistanceMatrix.getInstance(filterKeep);
        GenotypeDuplicity dupes = new GenotypeDuplicity(distMatrix, maxDistance);

        int subgroupsWithinTaxaGroups = dupes.getSubgroupsForListOfTaxaIndices(sampleMetadata.getTaxaGroupList(), GenotypeDuplicity.Method.EXACT_MATCH) * 2;
        int unmatches = sampleMetadata.getUnmatchingPhenotypesForAllDuplicates(dupes.getDuplicatedTaxa());
        //System.out.println(subgroupsWithinTaxaGroups + " + " + unmatches);
        return subgroupsWithinTaxaGroups + unmatches;
    }    
    
    public static void update(final EvolutionResult<EnumGene<Integer>, Integer> result){
        if (best == null || best.compareTo(result.getBestPhenotype()) < 0){
            best = result.getBestPhenotype();
            System.out.print(result.getGeneration() + ": ");
            System.out.println("Found best phenotype: "+ best);
            getBestMarkerInfo(best, PassParameters.getOutput());
        }else{
            System.out.println(result.getGeneration() + ": No improvement. Current best is "+ best.getFitness());
        }
    }
    
  
    public static void updateMinimize(final EvolutionResult<EnumGene<Integer>, Integer> result){
        if (best == null || best.compareTo(result.getBestPhenotype()) > 0){
            best = result.getBestPhenotype();
            System.out.print(result.getGeneration() + ": ");
            System.out.println("Found best phenotype: "+ best);
            getBestMarkerInfoWithPhenotypes(best, PassParameters.getOutput()); //Removed for changing nPoly to nNotPoly
            //getBestMarkerInfo(best, PassParameters.getOutput());
        }else{
            System.out.println(result.getGeneration() + ": No improvement. Current best is "+ best.getFitness());
        }
    }
    
    public static void updateTaxa(final EvolutionResult<EnumGene<Integer>, Integer> result){
        if (best == null || best.compareTo(result.getBestPhenotype()) < 0){
            best = result.getBestPhenotype();
            System.out.print(result.getGeneration() + ": ");
            System.out.println("Found best phenotype: "+ best);
            getBestTaxaInfo(best, PassParameters.getOutput());
        }else{
            System.out.println(result.getGeneration() + ": No improvement. Current best is "+ best.getFitness());
        }
    }
    
    public static void getBestMarkerInfo(final Phenotype<EnumGene<Integer>, Integer> result, String filename){
        Genotype<EnumGene<Integer>> gene = best.getGenotype();
        ArrayList<String> keepSites= new ArrayList<>();
        int[] markers = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
        for (int site = 0; site < markers.length; site++) {
            keepSites.add(genos.siteName(markers[site]));
        }
        GenotypeTable filterKeep= FilterGenotypeTable.getInstance(genos, keepSites.toArray(new String[keepSites.size()]));
        String name = filename;
        int lastIdx = name.lastIndexOf(".");
        if(lastIdx > 0){
            String ext = name.substring(lastIdx+1,name.length());
            if(ext.equals(new String("txt"))){
                name = name.substring(0, name.lastIndexOf("."));
            }
        }
        ExportUtils.writeToHapmap(filterKeep, name+".hmp.txt");
        File markerOutFile = new File(filename);
        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(filterKeep.positions()),"\t",markerOutFile);
        
//        DistanceMatrix distMatrix = IBSDistanceMatrix.getInstance(filterKeep);
//        int[] depth = new int[markers.length+1];
//        
//        for(int x = 0; x < filterKeep.numberOfTaxa(); x++){
//            double dist = 1.0;
//            for(int y = 0; y < filterKeep.numberOfTaxa(); y++){
//                double currentDist = distMatrix.getDistance(x, y);
//                if(dist > currentDist && x!=y) dist = currentDist;
//            }
////            System.out.println(dist);
//            depth[(int) Math.floor(dist*markers.length)]++;                
//        }
        
//        for(int x = 0; x < filterKeep.numberOfTaxa(); x++){
//            for(int y = 0; y < filterKeep.numberOfTaxa(); y++){
//                if(x != y){
//                    double dist = distMatrix.getDistance(x, y);
//                    depth[(int) Math.ceil(dist*markers.length)]++;
//                }else{
//                    System.out.println(distMatrix.getDistance(x, y));
//                }
//            }         
//        }
        
//        int[] cumulativeDepth = new int[markers.length+2];
//        
//        for (int i = depth.length-1; i >= 0; i--){
//            cumulativeDepth[i] = depth[i]+cumulativeDepth[i+1];
//        }
//        
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date dateobj = new Date();
        
//        List<String> headerForDepth = Arrays.asList("Depth"+"\t"+"Dist"+"\t"+"Total");
        Path file = Paths.get(filename);
        
//        try {
//            Files.write(file, headerForDepth, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
//        } catch (IOException e) {
//            System.err.println(e.getMessage());
//        }
//                     
//        for (int i = 0; i < depth.length; i++){
//            List<String> depthDistToPrint = Arrays.asList(i+"\t"+depth[i]+"\t"+cumulativeDepth[i]);
//            try {
//                Files.write(file, depthDistToPrint, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
//            } catch (IOException e) {
//                System.err.println(e.getMessage());
//            }
//        }

        List<String> lines = Arrays.asList(df.format(dateobj)+"\t"+ result.getGeneration() + ": "+result);
        
        try {
            Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        //return filterKeep.positions();
    }
    
    public static void getBestMarkerInfoWithPhenotypes(final Phenotype<EnumGene<Integer>, Integer> result, String filename){
        Genotype<EnumGene<Integer>> gene = best.getGenotype();
        ArrayList<String> keepSites= new ArrayList<>();
        int[] markers = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
        for (int site = 0; site < markers.length; site++) {
            keepSites.add(genos.siteName(markers[site]));
        }
        GenotypeTable filterKeep= FilterGenotypeTable.getInstance(genos, keepSites.toArray(new String[keepSites.size()]));
        String name = filename;
        int lastIdx = name.lastIndexOf(".");
        if(lastIdx > 0){
            String ext = name.substring(lastIdx+1,name.length());
            if(ext.equals(new String("txt"))){
                name = name.substring(0, name.lastIndexOf("."));
            }
        }
        ExportUtils.writeToHapmap(filterKeep, name+".hmp.txt");
        File markerOutFile = new File(filename);
        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(filterKeep.positions()),"\t",markerOutFile);
        
        File distMatOutFile = new File(name+".dist.csv");
        DistanceMatrix distMatrix = IBSDistanceMatrix.getInstance(filterKeep);
        TableReportUtils.saveDelimitedTableReport(distMatrix,",",distMatOutFile);
        
//        DistanceMatrix distMatrix = IBSDistanceMatrix.getInstance(filterKeep);
//        int[] depth = new int[markers.length+1];
//        
//        for(int x = 0; x < filterKeep.numberOfTaxa(); x++){
//            double dist = 1.0;
//            for(int y = 0; y < filterKeep.numberOfTaxa(); y++){
//                double currentDist = distMatrix.getDistance(x, y);
//                if(dist > currentDist && x!=y) dist = currentDist;
//            }
////            System.out.println(dist);
//            depth[(int) Math.floor(dist*markers.length)]++;                
//        }
        
//        for(int x = 0; x < filterKeep.numberOfTaxa(); x++){
//            for(int y = 0; y < filterKeep.numberOfTaxa(); y++){
//                if(x != y){
//                    double dist = distMatrix.getDistance(x, y);
//                    depth[(int) Math.ceil(dist*markers.length)]++;
//                }else{
//                    System.out.println(distMatrix.getDistance(x, y));
//                }
//            }         
//        }
        
//        int[] cumulativeDepth = new int[markers.length+2];
//        
//        for (int i = depth.length-1; i >= 0; i--){
//            cumulativeDepth[i] = depth[i]+cumulativeDepth[i+1];
//        }
//        
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date dateobj = new Date();
        
//        List<String> headerForDepth = Arrays.asList("Depth"+"\t"+"Dist"+"\t"+"Total");
        Path file = Paths.get(filename);
        
//        try {
//            Files.write(file, headerForDepth, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
//        } catch (IOException e) {
//            System.err.println(e.getMessage());
//        }
//                     
//        for (int i = 0; i < depth.length; i++){
//            List<String> depthDistToPrint = Arrays.asList(i+"\t"+depth[i]+"\t"+cumulativeDepth[i]);
//            try {
//                Files.write(file, depthDistToPrint, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
//            } catch (IOException e) {
//                System.err.println(e.getMessage());
//            }
//        }

        GenotypeDuplicity dupes = new GenotypeDuplicity(distMatrix, maxDistance);
        //ExportUtils.writeToHapmap(mergedGeno, PassParameters.getOutput()+".last.hmp.txt");

        int subgroupsWithinTaxaGroups = dupes.getSubgroupsForListOfTaxaIndices(sampleMetadata.getTaxaGroupList(), GenotypeDuplicity.Method.EXACT_MATCH);
        int unmatches = sampleMetadata.getUnmatchingPhenotypesForAllDuplicates(dupes.getDuplicatedTaxa());

        List<String> lines = Arrays.asList(df.format(dateobj)+"\t"+ result.getGeneration() + ": "+result);
        List<String> line2 = Arrays.asList("Same genotype with different subgroup: " + unmatches);
        List<String> line3 = Arrays.asList("Same subgroup with different genotypes: " + subgroupsWithinTaxaGroups);
        System.out.println("Same genotype with different subgroup: " + unmatches);
        System.out.println("Same subgroup with different genotypes: " + subgroupsWithinTaxaGroups);
        
        try {
            Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
            Files.write(file, line2, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
            Files.write(file, line3, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        //return filterKeep.positions();
    }
    
    public static int getPolymorphicSites(Genotype<EnumGene<Integer>> gene) {
        int[] taxaIndexToKeep = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
                
        Arrays.sort(taxaIndexToKeep);
        
        TaxaList taxa = genos.taxa();
        TaxaListBuilder tlb = new TaxaListBuilder();
        
        for (int i = 0; i < taxaIndexToKeep.length; i++){
            tlb.add(taxa.get(taxaIndexToKeep[i]));
        }
		
		if(numberOfControlTaxa > 0){
			for(int control = 0; control < numberOfControlTaxa; control++)
				tlb.add(taxa.get(totalNumberOfTaxa+control));
		}
        
        taxa = tlb.build();        

        //System.out.println("Out of "+genos.numberOfTaxa()+" removed "+(genos.numberOfTaxa()-taxaIndexToKeep.length)+" duplicate taxa");
        GenotypeTable filterKeep = FilterGenotypeTable.getInstance(genos, taxa);
        
        int polymorphicSites = 0;
        for (int site = 0; site < totalNumberOfMarkers; site++){
            if(filterKeep.minorAlleleFrequency(site) >= maxDistance) polymorphicSites++;
        }
        return polymorphicSites;
    }
    
    public static int getHeterozygousSites(Genotype<EnumGene<Integer>> gene) {
        int[] taxaIndexToKeep = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
                
        Arrays.sort(taxaIndexToKeep);
        
        TaxaList taxa = genos.taxa();
        TaxaListBuilder tlb = new TaxaListBuilder();
        
        for (int i = 0; i < taxaIndexToKeep.length; i++){
            tlb.add(taxa.get(taxaIndexToKeep[i]));
        }
                
                if(numberOfControlTaxa > 0){
                        for(int control = 0; control < numberOfControlTaxa; control++)
                                tlb.add(taxa.get(totalNumberOfTaxa+control));
                }
        
        taxa = tlb.build();        

        //System.out.println("Out of "+genos.numberOfTaxa()+" removed "+(genos.numberOfTaxa()-taxaIndexToKeep.length)+" duplicate taxa");
        GenotypeTable filterKeep = FilterGenotypeTable.getInstance(genos, taxa);
        
        int heterozygousSites = 0;
        for (int site = 0; site < totalNumberOfMarkers; site++){
            if((double)filterKeep.heterozygousCount(site)/(double)filterKeep.numberOfTaxa() >= maxDistance) heterozygousSites++;
        }
        return heterozygousSites;
    }
    
    public static void getBestTaxaInfo(final Phenotype<EnumGene<Integer>, Integer> result, String filename){
        Genotype<EnumGene<Integer>> gene = best.getGenotype();
        int[] taxaIndexToKeep = gene.getChromosome().stream().mapToInt(EnumGene<Integer>::getAllele).toArray();
        Arrays.sort(taxaIndexToKeep);
        
        TaxaList taxa = genos.taxa();
        TaxaListBuilder tlb = new TaxaListBuilder();
        
        for (int i = 0; i < taxaIndexToKeep.length; i++){
            tlb.add(taxa.get(taxaIndexToKeep[i]));
        }
		
        if(numberOfControlTaxa > 0){
			for(int control = 0; control < numberOfControlTaxa; control++)
				tlb.add(taxa.get(totalNumberOfTaxa+control));
		}
		
        taxa = tlb.build();   
        
        File markerOutFile = new File(filename);
        TableReportUtils.saveDelimitedTableReport(new TaxaListTableReport(taxa),"\t",markerOutFile);
        
        GenotypeTable filteredGeno = FilterGenotypeTable.getInstance(genos, taxa);
        
        //int array for MAFs {0.01, 0.05, 0.10, 0.20, 0.30, 0.40}
        double[] mafs = {0.0, 0.01, 0.05, 0.10, 0.20, 0.30, 0.40, 0.50, 1.0};
        int[] mafDist = new int[mafs.length];
        
        for (int i = 0; i < filteredGeno.numberOfSites(); i++){
            int mafIndex = 0;
            while(filteredGeno.minorAlleleFrequency(i) >= mafs[mafIndex])
                mafIndex++;
            mafDist[mafIndex-1]++;
        }
        
        int[] cumulativeMaf = new int[mafs.length];
        
        for (int i = cumulativeMaf.length-2; i >= 0; i--){
            cumulativeMaf[i] = mafDist[i]+cumulativeMaf[i+1];
        }
        
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date dateobj = new Date();
        
        List<String> headerForMafDist = Arrays.asList("MAF"+"\t"+"SNPs"+"\t"+"Dist");
        Path file = Paths.get(filename);
        
        try {
            Files.write(file, headerForMafDist, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
                     
        for (int i = 0; i < mafs.length-1; i++){
            List<String> mafDistToPrint = Arrays.asList(mafs[i]+"\t"+cumulativeMaf[i]+"\t"+mafDist[i]);
            try {
                Files.write(file, mafDistToPrint, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
            
        List<String> lines = Arrays.asList(df.format(dateobj)+"\t"+ result.getGeneration() + ": "+result);
        try {
            Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        //return filterKeep.positions();
    }
    
//    public static void main(String[] argv) throws IOException {
//        int selectNumberOfMarkers = 10;
//        
//        if (argv.length == 2) {
//            try {
//                selectNumberOfMarkers = Integer.parseInt(argv[0]);
//            } catch(NumberFormatException e) {
//              System.err.println("Number " + argv[0] + " invalid (" + e.getMessage() + ").");
//              System.exit(1);
//            }
//          } else {
//            System.err.println("usage: <Number of target markers> <filename>");
//            System.exit(1);
//          }
//        
//        
//        // 3.) Create the execution environment.
//        final Engine<EnumGene<Integer>, Integer> engine = Engine
//            .builder(GeneticAlgorithm::getUniques, PermutationChromosome.ofInteger(IntRange.of(0,totalNumberOfMarkers-1), selectNumberOfMarkers))
//            .optimize(Optimize.MAXIMUM) 
//            .maximalPhenotypeAge(7) 
//            .populationSize(100000)
//            .offspringFraction(0.7)
//            .survivorsSelector(new RouletteWheelSelector<>())
////            .offspringSelector(new RouletteWheelSelector<>())
//            .alterers( 
//                new Mutator<>(0.01), 
//                new PartiallyMatchedCrossover<>(0.35)
//                ) 
//            .build();
// 
//        // 4.) Start the execution (evolution) and
//        //     collect the result.  
//        final EvolutionStatistics<Integer, ?> 
//        statistics = EvolutionStatistics.ofNumber(); 
//        
//        final Phenotype<EnumGene<Integer>, Integer> result = engine.stream()
//            .limit(bySteadyFitness(11)) 
//            .limit(100)
//            .peek(statistics)
//            .collect(toBestPhenotype());
// 
//        System.out.println(statistics);
//        System.out.println("Result:\n" + result);
//    }
}