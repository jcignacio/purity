package purity.geneticAlgorithm;

import net.maizegenetics.dna.snp.GenotypeTable;

public class PassParameters {
    private static String filename;
    private static String output;
    private static int totalNumberOfMarkers;
    private static int numberOfControlTaxa;
    private static double maxDistance;
    private static int nPoly;
    private static GenotypeTable genos;
    private static GenotypeTable genoToBackup;
    private static SampleMetadata sampleMetadata;
    
    
    public static void setFilename(String input){
        filename = input;
    }
    
    public static String getFilename(){
        return filename;
    }
    
    public static void setOutput(String input){
        output = input;
    }
    
    public static String getOutput(){
        return output;
    }
    
    public static void setTotalNumberOfMarkers(int input){
        totalNumberOfMarkers = input;
    }
    
    public static int getTotalNumberOfMarkers(){
        return totalNumberOfMarkers;
    }
    
    public static void setNumberOfControlTaxa(int input){
        numberOfControlTaxa = input;
    }
    
    public static int getNumberOfControlTaxa(){
        return numberOfControlTaxa;
    }
    
    public static void setMaxDistance(double input){
        maxDistance = input;
    }
    
    public static double getMaxDistance(){
        return maxDistance;
    }
    
    public static void setNPoly(int input){
        nPoly = input;
    }
    
    public static int getNPoly(){
        return nPoly;
    }

    public static void setGenoToBackup(GenotypeTable input) {
        genoToBackup = input;
    }
    
    public static GenotypeTable getGenoToBackup() {
        return genoToBackup;
    }

    public static void setGenos(GenotypeTable input) {
        genos = input;
    }
    
    public static GenotypeTable getGenos() {
        return genos;
    }

    public static void setSampleMetadata(SampleMetadata input) {
        sampleMetadata = input;
    }
    
    public static SampleMetadata getSampleMetadata() {
        return sampleMetadata;
    }
}
