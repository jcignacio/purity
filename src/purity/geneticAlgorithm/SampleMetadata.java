package purity.geneticAlgorithm;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import net.maizegenetics.phenotype.Phenotype;
import net.maizegenetics.phenotype.PhenotypeBuilder;

public class SampleMetadata {
    private Phenotype pheno;
    private Map<String, Integer> dict;
    private ArrayList<ArrayList<Integer>> taxaGroupList;

    public SampleMetadata(String filename) {
        
        PhenotypeBuilder pb = new PhenotypeBuilder().fromFile(filename);
        List<Phenotype> lp = pb.build();
        pheno = lp.get(0);
        
        // Make hash for phenotype
        dict = new HashMap<String, Integer>();
        
        for(int i = 0; i < pheno.taxa().size(); i++) {
            dict.put(pheno.value(i, 0).toString(), Integer.parseInt(pheno.value(i, 1).toString()));      
        }
    }
    
    public SampleMetadata(String filename, int groupIndex) {
        
        PhenotypeBuilder pb = new PhenotypeBuilder().fromFile(filename);
        List<Phenotype> lp = pb.build();
        pheno = lp.get(0);
        
        // Make hash for phenotype and taxa group
        dict = new HashMap<String, Integer>();
        Map<String, ArrayList<Integer>> groupDict = new HashMap<String, ArrayList<Integer>>();
        
        
        for(int i = 0; i < pheno.taxa().size(); i++) {
            String taxaName = pheno.value(i, 0).toString();
            String group = pheno.value(i, 1).toString();
            dict.put(taxaName, Integer.parseInt(group));
            if (groupDict.containsKey(group)) {
                groupDict.get(group).add(i);
            } else {
                groupDict.put(group, new ArrayList<Integer>());
                groupDict.get(group).add(i);
            }
        }
        
        ArrayList<ArrayList<Integer>> taxaGroupList = new ArrayList<ArrayList<Integer>>();
        
        for(Map.Entry<String, ArrayList<Integer>> entry : groupDict.entrySet()) {
            taxaGroupList.add(entry.getValue());
        }
        
//        for(int i=0;i<taxaGroupList.size();i++) {
//        	for(int j=0;j<taxaGroupList.get(i).size();j++) {
//        		System.out.print(taxaGroupList.get(i).get(j)+" ");
//        	}
//        	System.out.print("\n");
//        }
        
        this.taxaGroupList = taxaGroupList;
    }
    
    public Map getDictionary(){
        return dict;
    }
    
    public ArrayList<ArrayList<Integer>> getTaxaGroupList(){
        return taxaGroupList;
    }
    
    public int getUnmatchingPhenotypes(List<String> taxaNames) {
        Integer numberOfGroups = 0;
        Integer unmatches = 1;
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        
        for(int i = 0; i < taxaNames.size(); i++) {
            Integer group = dict.get(taxaNames.get(i));
            if(map.containsKey(group)) {
                Integer val = map.get(group);
                map.put(group, val + 1);
            }
            else {
                map.put(group, 1);
                numberOfGroups++;
            }
        }
        
        if (numberOfGroups == 1) {
            return 0;
        } else {
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                unmatches = unmatches * entry.getValue();
            }
            return unmatches;
        }        
    }
    
    public int getUnmatchingPhenotypes(ArrayList<Integer> taxaIndices) {
        Integer numberOfGroups = 0;
        Integer unmatches = 1;
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        
        for(int i = 0; i < taxaIndices.size(); i++) {
            Integer group = Integer.parseInt(pheno.getValueAt(taxaIndices.get(i), 1).toString());
            //System.out.print(taxaIndices.get(i)+":"+pheno.getValueAt(taxaIndices.get(i), 0) + ":" + pheno.getValueAt(taxaIndices.get(i), 1) +" " );
            if(map.containsKey(group)) {
                Integer val = map.get(group);
                map.put(group, val + 1);
            }
            else {
                map.put(group, 1);
                numberOfGroups++;
            }
        }
        //System.out.println("");
        //System.out.println("number of groups: "+numberOfGroups);
        if (numberOfGroups == 1) {
            return 0;
        } else {
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                //System.out.print(entry.getKey() + ": " + entry.getValue() + " ");
                unmatches =  unmatches + entry.getValue();
                //if (unmatches > 10000) break;
            }
            //System.out.println();
            //System.out.println(unmatches);
            return unmatches * (map.size()-1);
        }        
    }
    
    public int getUnmatchingPhenotypesForAllDuplicates(ArrayList<ArrayList<Integer>> dupeList) {
        Integer score = 0;
        
        //System.out.println("List size: " + dupeList.size());
        
        for(int i = 0; i < dupeList.size(); i++) {
            if(dupeList.get(i).size() > 1) {
                //System.out.println("score: " + getUnmatchingPhenotypes(dupeList.get(i)) );
                score = score + getUnmatchingPhenotypes(dupeList.get(i));
            }
        }
        
        return score;
    }
    
    
}
