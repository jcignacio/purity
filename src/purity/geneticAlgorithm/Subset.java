package purity.geneticAlgorithm;

import static java.lang.Math.PI; 
import static java.lang.Math.abs; 
import static java.lang.Math.sin; 
import static io.jenetics.engine.EvolutionResult.toBestPhenotype; 
import io.jenetics.engine.Limits;
import io.jenetics.engine.Problem;

import java.util.function.Function;
import java.util.stream.IntStream; 
 
import io.jenetics.EnumGene; 
import io.jenetics.Optimize; 
import io.jenetics.PartiallyMatchedCrossover; 
import io.jenetics.Phenotype; 
import io.jenetics.SwapMutator; 
import io.jenetics.engine.Engine; 
import io.jenetics.engine.EvolutionStatistics;
import io.jenetics.engine.Codec;
import io.jenetics.engine.Codecs; 
import io.jenetics.util.ISeq;
 
public class Subset implements Problem<ISeq<Integer>, EnumGene<Integer>, Integer> {
    private final ISeq<Integer>_basicSet;
    private final int _size;

    public Subset(ISeq<Integer> basicSet, int size){
        _basicSet=basicSet;
        _size=size;
    }
    
    @Override
    public Function<ISeq<Integer>, Integer> fitness() {
        return subset −> abs(
                subset.stream().mapToInt(Integer::intValue).sum());
    }
    
    @Override
    public Codec<ISeq<Integer>, EnumGene<Integer>> codec() {
        return Codecs.ofSubSet(_basicSet,_size);
    }

    public static void main(String[] args){
        
    }
}