/*
 * Java Genetic Algorithm Library (@__identifier__@). 
 * Copyright (c) @__year__@ Franz Wilhelmstötter 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License. 
 * 
 * Author: 
 *    Franz Wilhelmstötter (franz.wilhelmstoetter@gmx.at) 
 */
package purity.geneticAlgorithm; 
 
import java.awt.Dimension; 
import java.util.Random; 
 
import org.jenetics.AnyGene;
import org.jenetics.Optimize;
import org.jenetics.Phenotype; 
import org.jenetics.engine.Engine; 
import org.jenetics.engine.EvolutionResult;
import org.jenetics.engine.EvolutionStatistics;
import org.jenetics.engine.codecs; 
import org.jenetics.util.RandomRegistry; 
 
/**
 * @author <a href="mailto:franz.wilhelmstoetter@gmx.at">Franz Wilhelmstötter</a> 
 */ 
public class Squares { 
 
 private static Dimension nextDimension() { 
  final Random random = RandomRegistry.getRandom(); 
  Dimension dim = new Dimension(random.nextInt(100), random.nextInt(100));
  System.out.println(dim.getHeight());
  return dim;
 } 
 
 private static double area(final Dimension dim) { 
  return dim.getHeight()*dim.getWidth(); 
 } 
 
 public static void main(final String[] args) { 
  final Engine<AnyGene<Dimension>, Double> engine = Engine 
   .builder(Squares::area, codecs.ofScalar(Squares::nextDimension)) 
   .optimize(Optimize.MAXIMUM)
   //.populationSize(100)
   .build(); 
  
  final EvolutionStatistics<Double, ?> 
   statistics = EvolutionStatistics.ofNumber(); 
  
  final Phenotype<AnyGene<Dimension>, Double> pt = engine.stream() 
   .limit(100)
   .peek(statistics) 
   .collect(EvolutionResult.toBestPhenotype()); 
 
  System.out.println(statistics); 
  System.out.println(pt); 
 } 
 
}