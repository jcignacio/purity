package purity.geneticAlgorithm;

import static io.jenetics.engine.EvolutionResult.toBestPhenotype;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import io.jenetics.EnumGene;
import io.jenetics.Mutator;
import io.jenetics.Optimize;
import io.jenetics.PartiallyMatchedCrossover;
import io.jenetics.PermutationChromosome;
import io.jenetics.Phenotype;
import io.jenetics.RouletteWheelSelector;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionStatistics;
import io.jenetics.engine.Limits;
import io.jenetics.util.IntRange;
import purity.geneticAlgorithm.GeneticAlgorithm;

import net.maizegenetics.dna.map.PositionList;
import net.maizegenetics.dna.map.PositionListTableReport;
import net.maizegenetics.util.TableReportUtils;

public class MaximumTaxaDiversity {
    public static void main(String[] argv) throws IOException{
        int selectNumberOfMarkers = 10;
        int populationSize = 100;
        double maxDistance = 0.0;
        String filename = "C:/Users/jignacio/Google Drive/Trainings/R/purity/skim_and_3k_elite_core.hmp.txt";
        String output = "C:/Users/jignacio/Google Drive/Trainings/R/purity/skim_and_3k_elite_core.hmp.txt.purity.out";
        
        if (argv.length == 5) {
            try {
                selectNumberOfMarkers = Integer.parseInt(argv[0]);
                populationSize = Integer.parseInt(argv[1]);
                maxDistance = Double.parseDouble(argv[2]);
                filename = argv[3];
                output = argv[4];
            } catch(NumberFormatException e) {
              System.err.println("Arguments invalid (" + e.getMessage() + ").");
              System.exit(1);
            }
        }
        else {
            System.err.println("usage: <Number of target markers> <Population size> <distance> <input hmp.txt> <output file>");
            System.exit(1);
        }
        
//        selectNumberOfMarkers = 93;
//        populationSize = 100;
//        maxDistance = 0.05;
//        filename = "C:/Users/jignacio/Downloads/3k7k.hmp.txt.gz";
//        output = "C:/Users/jignacio/Downloads/3k7k.out";
    
        PassParameters.setNumberOfControlTaxa(0);
        PassParameters.setMaxDistance(maxDistance);
        PassParameters.setFilename(filename);
        PassParameters.setOutput(output);
        
        // 3.) Create the execution environment.
//        final Engine<EnumGene<Integer>, Integer> engine = Engine
//            .builder(GeneticAlgorithm::getUniques, PermutationChromosome.ofInteger(IntRange.of(0,GeneticAlgorithm.getTotalNumberOfMarkers()-1), selectNumberOfMarkers))
//            .optimize(Optimize.MAXIMUM) 
//            .maximalPhenotypeAge(7) 
//            .populationSize(populationSize)
//            .offspringFraction(0.7)
//            .survivorsSelector(new RouletteWheelSelector<>())
////            .offspringSelector(new RouletteWheelSelector<>())
//            .alterers( 
//                new Mutator<>(0.01), 
//                new PartiallyMatchedCrossover<>(0.35)
//                ) 
//            .build();
 
        
        final Engine<EnumGene<Integer>, Integer> engine = Engine
//                .builder(GeneticAlgorithm::getPolymorphicSites, PermutationChromosome.ofInteger(IntRange.of(0,GeneticAlgorithm.getTotalNumberOfMarkers()-1), selectNumberOfMarkers))
//                .builder(GeneticAlgorithm::getPolymorphicSites, PermutationChromosome.ofInteger(IntRange.of(0,GeneticAlgorithm.getTotalNumberOfTaxa()-1), selectNumberOfMarkers))
                .builder(GeneticAlgorithm::getHeterozygousSites, PermutationChromosome.ofInteger(IntRange.of(0,GeneticAlgorithm.getTotalNumberOfTaxa()-1), selectNumberOfMarkers))
                .optimize(Optimize.MAXIMUM) 
                .maximalPhenotypeAge(7) 
                .populationSize(populationSize)
                .offspringFraction(0.7)
                .survivorsSelector(new RouletteWheelSelector<>())
//                .offspringSelector(new RouletteWheelSelector<>())
                .alterers( 
//                    new Mutator<>(0.01), 
                    new PartiallyMatchedCrossover<>(0.35)
                    ) 
                .build();
        
        
        // 4.) Start the execution (evolution) and
        //     collect the result.  
        final EvolutionStatistics<Integer, ?> 
        statistics = EvolutionStatistics.ofNumber(); 
        
        final Phenotype<EnumGene<Integer>, Integer> result = engine.stream()
            //.limit(limit.byExecutionTime(Duration.ofMinutes(10)))
            .limit(Limits.bySteadyFitness(11))
            //.limit(limit.byFitnessThreshold(1651))
            .limit(100)
            .peek(GeneticAlgorithm::updateTaxa)
            .peek(statistics)
            .collect(toBestPhenotype());
        
        
//        Path file = Paths.get(output+".pur.scr");
//        File markerOutFile = new File(output+".pur.snp");

//        File markerOutFile = new File(output);
//        PositionList pl = GeneticAlgorithm.getBestMarkerInfo(result);

//        File purityScore = new File(output+".pur.scr");
//
//        if(purityScore.exists()){
//            try {
//                Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
//            }catch (IOException e) {
//                System.err.println(e.getMessage());
//            }
//        }else{
//                Files.write(file, lines, Charset.forName("UTF-8"));
//        }
//        
//        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(pl),"\t",markerOutFile); 
        
        System.out.println(statistics);
        System.out.println("Result:\n" + result);
        System.out.println("Results written to:");
        System.out.println(output);
    }
}
