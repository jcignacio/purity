package purity.geneticAlgorithm;

import static io.jenetics.engine.EvolutionResult.toBestPhenotype;

import java.io.IOException;

import io.jenetics.EnumGene;
import io.jenetics.Optimize;
import io.jenetics.PartiallyMatchedCrossover;
import io.jenetics.PermutationChromosome;
import io.jenetics.Phenotype;
import io.jenetics.RouletteWheelSelector;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionStatistics;
import io.jenetics.engine.Limits;
import io.jenetics.util.IntRange;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;

public class SubpopSpecificMarkers {

    public static void main(String[] argv) throws IOException {
        // TODO Auto-generated method stub
        int selectNumberOfMarkers = 10;
        int populationSize = 100;
        double maxDistance = 0.0;
        String filename = "C:/Users/jignacio/Google Drive/Trainings/R/purity/skim_and_3k_elite_core.hmp.txt";
        String trait_filename = "C:/Users/jignacio/Google Drive/Trainings/R/purity/skim_and_3k_elite_core.hmp.txt";
        String output = "C:/Users/jignacio/Google Drive/Trainings/R/purity/skim_and_3k_elite_core.hmp.txt.purity.out";
        
        if (argv.length == 6) {
            try {
                selectNumberOfMarkers = Integer.parseInt(argv[0]);
                populationSize = Integer.parseInt(argv[1]);
                maxDistance = Double.parseDouble(argv[2]);
                filename = argv[3];
                output = argv[4];
                trait_filename = argv[5];
            } catch(NumberFormatException e) {
              System.err.println("Arguments invalid (" + e.getMessage() + ").");
              System.exit(1);
            }
        }
        else {
            System.err.println("Usage: <No. of target markers> <Solution size> <Distance of duplicates> <Input HapMap file> <Output text file> <Input grouping file>\n");
            System.err.println("Example: java -jar purity.jar 10 1000 0.05 test.hmp.txt test.out.txt trait.txt\n");
            System.err.println("PARAMETERS:");
            System.err.println("<No. of target markers> - Integer\n\t"
                    + "Total no. markers to select among given dataset.\n");
            System.err.println("<Solution size> - Integer\n\t"
                    + "Number of solutions to consider.\n\t"
                    + "Higher size yields better results but takes longer time.\n");
            System.err.println("<Distance of duplicates> - Decimal (0.00-1.00)\n\t"
                    + "Min. distance bet. two samples before considering as duplicates.\n\t"
                    + "Set to 0.00 for exact match.\n");
            System.err.println("<Input HapMap file> - HapMap file (*.hmp.txt, *.hmp.txt.gz)\n\t"
                    + "Dataset to choose markers from.\n\t"
                    + "Supports either IUPAC or diploid format.\n");
            System.err.println("<Output text file> - Text file\n\t"
                    + "File to write output score and info of current best set of markers.\n\t"
                    + "The score is the no. of uniquely identified samples.\n\t"
                    + "Also outputs a subset HapMap file.\n");
            System.err.println("<Input grouping file> - Text file\n\t"
                    + "Grouping information in TASSEL phenotype file format.\n\t");
            System.exit(1);
        }
        

        PassParameters.setMaxDistance(maxDistance);
        PassParameters.setFilename(filename);
        GenotypeTable genos = ImportUtils.read(filename);
        SampleMetadata sm = new SampleMetadata(trait_filename, 0); 
        PassParameters.setGenos(genos);
        PassParameters.setSampleMetadata(sm);
        PassParameters.setOutput(output);
        
        // 3.) Create the execution environment.
        final Engine<EnumGene<Integer>, Integer> engine = Engine
            .builder(GeneticAlgorithm::getDuplicateGenosBetweenTraitGroups, PermutationChromosome.ofInteger(IntRange.of(0,GeneticAlgorithm.getTotalNumberOfMarkers()), selectNumberOfMarkers))
            .optimize(Optimize.MINIMUM) 
            .maximalPhenotypeAge(7) 
            .populationSize(populationSize)
            .offspringFraction(0.7)
            .survivorsSelector(new RouletteWheelSelector<>())
//            .offspringSelector(new RouletteWheelSelector<>())
            .alterers( 
//                new Mutator<>(0.01), 
                new PartiallyMatchedCrossover<>(0.35)
                ) 
            .build();
 
        // 4.) Start the execution (evolution) and
        //     collect the result.  
        final EvolutionStatistics<Integer, ?> 
        statistics = EvolutionStatistics.ofNumber(); 
        
        final Phenotype<EnumGene<Integer>, Integer> result = engine.stream()
            //.limit(Limits.byExecutionTime(Duration.ofMinutes(10)))
            .limit(Limits.bySteadyFitness(11))
            //.limit(Limits.byFitnessThreshold(1651))
            .limit(100)
            .peek(GeneticAlgorithm::updateMinimize)
            .peek(statistics)
            .collect(toBestPhenotype());
        
        
//        Path file = Paths.get(output+".pur.scr");
//        File markerOutFile = new File(output+".pur.snp");

//        File markerOutFile = new File(output);
//        PositionList pl = GeneticAlgorithm.getBestMarkerInfo(result);

//        File purityScore = new File(output+".pur.scr");
//
//        if(purityScore.exists()){
//            try {
//                Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
//            }catch (IOException e) {
//                System.err.println(e.getMessage());
//            }
//        }else{
//                Files.write(file, lines, Charset.forName("UTF-8"));
//        }
//        
//        TableReportUtils.saveDelimitedTableReport(new PositionListTableReport(pl),"\t",markerOutFile); 
        
        System.out.println(statistics);
        System.out.println("Result:\n" + result);
        System.out.println("Results written to:");
        System.out.println(output);
    }

}
