package purity.geneticAlgorithm;

import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.dna.snp.ExportUtils;

public class RemoveDuplicates {
    private static String genotypeFile;
    private static String phenotypeFile;
    private static String outputFile;
    private static Double distance;
    private static GenotypeTable genos;
    private static GenotypeDuplicity dupes;
    private static GenotypeTable filteredGenos;

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        genotypeFile = args[0];
        outputFile = args[1];
        distance = Double.parseDouble(args[2]);
        phenotypeFile = args[3];

        SampleMetadata sampleMetadata = new SampleMetadata(phenotypeFile, 0);
        
        genos = ImportUtils.readFromHapmap(genotypeFile);
        
        dupes = new GenotypeDuplicity(genos, distance);
        
        filteredGenos = dupes.removeDuplicateTaxa(genos);
        
        dupes.printDuplicatedTaxa(genos);
        dupes.printFullDuplicityInfo();
        
        int subgroupsWithinTaxaGroups = dupes.getSubgroupsForListOfTaxaIndices(sampleMetadata.getTaxaGroupList(), GenotypeDuplicity.Method.EXACT_MATCH);
        int unmatches = sampleMetadata.getUnmatchingPhenotypesForAllDuplicates(dupes.getDuplicatedTaxa());
        
        ExportUtils.writeToHapmap(filteredGenos, outputFile);
        
    }

}
