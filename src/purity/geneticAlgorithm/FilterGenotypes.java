/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package purity.geneticAlgorithm;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import net.maizegenetics.dna.snp.ExportUtils;
import net.maizegenetics.dna.snp.FilterGenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTableBuilder;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.TaxaListBuilder;
import net.maizegenetics.taxa.Taxon;
import net.maizegenetics.util.Utils;
/**
 *
 * @author kls283
 */
public class FilterGenotypes {

    public static GenotypeTable sampleSitesByMAF(String inFile, double minMAF, double maxMAF) {
        GenotypeTable genos= ImportUtils.readFromHapmap(inFile);
        System.out.println("Read in genotypeTable file"+inFile);
        ArrayList<String> keepSites= new ArrayList<>();
        ArrayList<Integer> removeSites= new ArrayList<>();

        for (int site = 0; site < genos.numberOfSites(); site++) {
            double currMAF= genos.minorAlleleFrequency(site);
            if (currMAF>=minMAF && currMAF<=maxMAF) keepSites.add(genos.siteName(site));
            else removeSites.add(site);
        }
        
        System.out.println("Out of "+genos.numberOfSites()+" keep "+keepSites.size()+" sites");
        GenotypeTable filterKeep= FilterGenotypeTable.getInstance(genos, keepSites.toArray(new String[keepSites.size()]));
//        String outFile= inFile.substring(0, inFile.indexOf(".hmp"))+"InclusiveByMAF_"+minMAF+"-"+maxMAF+".vcf";
//        ExportUtils.writeToHapmap(genos, outFile);
//
        String outputToText= inFile.substring(0, inFile.indexOf(".hmp"))+"ExcludedSites_"+minMAF+"-"+maxMAF+".txt";
//        writeExcludedSitesToFile(genos, removeSites, outputToText);
        return filterKeep;
    }
    
//    public static void generateLD(String inFile, double minMAF, double maxMAF) {
//        GenotypeTable genos= ImportUtils.readFromHapmap(inFile);
//        System.out.println("Read in genotypeTable file"+inFile);
//        new LinkageDisequilibrium(genos);
//        
//    }
    
    public static GenotypeTable filterByChromosomePosRange(GenotypeTable genos, String chromosome, int position, int range, boolean keep){
        ArrayList<String> keepSites= new ArrayList<>();
        ArrayList<String> removeSites= new ArrayList<>();

        int maxRange = position + range;
        int minRange;
        
        if(position > range) minRange = position - range;
        else minRange = 0;
        
        int i = 0;
        while((i+1) < genos.numberOfSites() && Integer.parseInt(genos.chromosomeName(i)) != Integer.parseInt(chromosome)){
            //if(genos.chromosomeName(i) == chromosome) break;
            System.out.println(genos.chromosomeName(i)+"\t"+chromosome+"\t"+i);
            i++;
        }
        
        while((i+1) < genos.numberOfSites() && Integer.parseInt(genos.chromosomeName(i)) == Integer.parseInt(chromosome)){
            System.out.println(genos.chromosomeName(i)+"\t"+chromosome+"\t"+i);
            if(genos.chromosomalPosition(i) > minRange && genos.chromosomalPosition(i) < maxRange) keepSites.add(genos.siteName(i));
            else removeSites.add(genos.siteName(i));
            i++;
        }
        
        if(!keep) keepSites = removeSites;
        
        System.out.println("Out of "+genos.numberOfSites()+" keep "+keepSites.size()+" sites");
        GenotypeTable filterKeep= FilterGenotypeTable.getInstanceRemoveSiteNames(genos, keepSites.toArray(new String[keepSites.size()]));
        return filterKeep;
//        String outFile= inFile.substring(0, inFile.indexOf(".hmp"))+"excluded_near_straits.hmp";
//        ExportUtils.writeToHapmap(genos, outFile);
        
//        String outputToText= inFile.substring(0, inFile.indexOf(".hmp"))+"ExcludedSites_"+minMAF+"-"+maxMAF+".txt";
    }

    public static void writeExcludedSitesToFile(GenotypeTable genos, ArrayList<Integer> removeSites, String outFile) {
        try{
            DataOutputStream outStream= Utils.getDataOutputStream(outFile, 1000);
            outStream.writeBytes("Chromosome\tPhysicalPosition\tMAF\n");
            for (int site:removeSites) {
                outStream.writeBytes(genos.chromosomeName(site)+"\t"+genos.physicalPositions()[site]+"\t"+genos.minorAlleleFrequency(site)+"\n");
            }
            outStream.close();
        }
        catch (Exception e) {
            System.out.println("Problem writing output: "+e);
        }
    }
    
    public static GenotypeTable removeDuplicateTaxa(GenotypeTable genos, double maxDistance){
        GenotypeDuplicity duplicity = new GenotypeDuplicity(genos, maxDistance);
        int taxaWithMostNonMissingSite = 0;
        ArrayList<ArrayList<Integer>> duplicatedTaxa = duplicity.getDuplicatedTaxa();
        int[] taxaIndexToKeep = new int[duplicatedTaxa.size()];
        
        for(int i = 0; i < duplicatedTaxa.size(); i++){ // for each group of taxa with same genotypes
            ArrayList<Integer> currentGroup = duplicatedTaxa.get(i);
            int maxNonMissingSite = 0;
            for(int j = 0; j < currentGroup.size(); j++){ //get taxa with least missing calls
                int currentTaxon = currentGroup.get(j);
                int currentNonMissingSites = genos.totalNonMissingForTaxon(currentTaxon);
                    if(maxNonMissingSite < currentNonMissingSites){
                        maxNonMissingSite = currentNonMissingSites;
                        taxaWithMostNonMissingSite = currentTaxon;
                    }
            }
            taxaIndexToKeep[i] = taxaWithMostNonMissingSite;
        }
        
        Arrays.sort(taxaIndexToKeep);
        
        TaxaList taxa = genos.taxa();
        TaxaListBuilder tlb = new TaxaListBuilder();
        
        for (int i = 0; i < taxaIndexToKeep.length; i++){
            tlb.add(taxa.get(taxaIndexToKeep[i]));
        }
        
        taxa = tlb.build();        

        System.out.println("Out of "+genos.numberOfTaxa()+" removed "+(genos.numberOfTaxa()-taxaIndexToKeep.length)+" duplicate taxa");
        GenotypeTable filterKeep = FilterGenotypeTable.getInstance(genos, taxa);
        return filterKeep;
    }
    
    
    public static void filterHapmap(String[] args) {
        String inFile= "C:/Users/jignacio/Google Drive/Trainings/R/purity/TRB/oytpyt.hmp.txt";
        String siteFile = "C:/Users/jignacio/Google Drive/Trainings/R/purity/straits_marker_locs.txt";
        int range = 1000000;
        
        if (args.length == 3) {
                inFile= args[0];
                siteFile = args[1];
                range = Integer.parseInt(args[2]);
        }
        else {
            System.err.println("usage: <Input HapMap file> <Targets file in 'Chr <TAB> Pos' format > <Range within pos in bp to exclude>");
            System.exit(1);
        }
        
        GenotypeTable genos = ImportUtils.readFromHapmap(inFile);
             
        String outFile= inFile.substring(0, inFile.indexOf(".hmp"))+"excluded_near_straits";
        
        // If included in an Eclipse project.
        
        BufferedReader buffer = null;
        try {
            buffer = new BufferedReader(new FileReader(siteFile));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String line;
        try {
            while ((line = buffer.readLine()) != null) {
                String[] vals = line.trim().split("\\t");
                genos = filterByChromosomePosRange(genos,vals[0],Integer.parseInt(vals[1]),range, true);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
            ExportUtils.writeToHapmap(genos, outFile);
    }
    
    public static void removeDupes(){
        String inFile= "C:\\Users\\jignacio\\Downloads\\ind_core_ld_prune_excluded_near_straits_nohets.hmp.txt.gz";
        
        GenotypeTable genos = ImportUtils.readFromHapmap(inFile);
             
        String outFile= inFile.substring(0, inFile.indexOf(".hmp"))+"_0.01_dist";
        
        //genos = GenotypeTableBuilder.getHomozygousInstance(genos);
        genos = removeDuplicateTaxa(genos, 0.01);
        // If included in an Eclipse project.

        ExportUtils.writeToHapmap(genos, outFile);
    }
    
    public static void main(String[] args) {
        //String dir="C:/Users/jignacio/Google Drive/Trainings/R/purity/TRB/";
        //String inFile= dir+"oytpyt.hmp.txt";
        //removeDupes();
        //GenotypeTable genos= ImportUtils.readFromHapmap(inFile);
//        GenotypeTable genos = sampleSitesByMAF(inFile, 0.05, 0.1);
//        
//              
//        DistanceMatrix distMatrix = IBSDistanceMatrix.getInstance(genos);
//        int zeroes = 0;
//        
//        for (int i = 0; i < genos.numberOfTaxa(); i++) {
//            for (int j = 0; j < i; j++) {
//                float dist = distMatrix.getDistance(i, j);
//                if (dist == 0){
//                    zeroes++;
//                    System.out.println(dist+"\t"+genos.taxaName(i)+"\t"+genos.taxaName(j));
//                }
//            }
//        }
//        System.out.println(zeroes);
    }

}