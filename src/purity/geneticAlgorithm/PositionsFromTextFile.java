package purity.geneticAlgorithm;

public class PositionsFromTextFile {
    String chromosome;
    int position;
    
    public String getChromosome(){
        return chromosome;
    }
    
    public int getPosition(){
        return position;
    }
    
    public PositionsFromTextFile(String chromosome, int position){
        this.chromosome = chromosome;
        this.position = position;
    }
}