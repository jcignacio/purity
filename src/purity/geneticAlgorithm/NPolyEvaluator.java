package purity.geneticAlgorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import net.maizegenetics.dna.snp.FilterGenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;

public class NPolyEvaluator {
	private static final int[] result8 = {9,3,1,14,10,4,0,5,13,2,7,12,6,11}; 
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String markers = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14";
		String polySNPsFile = "/home/carli/workspace/osativa-kasp-analysis/subpop-analysis/accession_level_genotyping_sorted.hmp.txt";
		
		if (args.length != 0) {
			markers = args[1];
			polySNPsFile = args[0];
		}
		
		String[] strArray = markers.split(",");
		int[] intArray = new int[strArray.length];
		for(int i = 0; i < strArray.length; i++) {
		    intArray[i] = Integer.parseInt(strArray[i]);
		}
		
		GenotypeTable geno = ImportUtils.readFromHapmap(polySNPsFile);
		
        ArrayList<String> siteNamesToKeepList = new ArrayList<String>();
        
        Arrays.sort(intArray);
        
        for (int i = 0; i < intArray.length; i++){
            //siteNamesToKeep[i] = geno.siteName(result5[i]);
            //siteNamesToKeepList.add(String.format("chr%02d", Integer.parseInt(geno.chromosomeName(result5[i])))+":"+geno.chromosomalPosition(result5[i]));
            siteNamesToKeepList.add(geno.siteName(intArray[i]));
            //System.out.println(siteNamesToKeepList.get(i));
        }
        
        GenotypeTable geno2 = FilterGenotypeTable.getInstance(geno, siteNamesToKeepList);
        
		
		PairwisePolymorphism pp1 = new PairwisePolymorphism(geno2);
//        System.out.println("Remaining lines without 1 polymorphic SNPs: " + pp1.remainingLinesWithoutPolySNPs(1));
//        System.out.println("Remaining lines without 2 polymorphic SNPs: " + pp1.remainingLinesWithoutPolySNPs(2));
        System.out.println(pp1.remainingLinesWithoutPolySNPs(1) + " " + pp1.remainingLinesWithoutPolySNPs(2));
	}
	
	public static List<Integer> pickNRandom(List<Integer> lst, int n) {
	    List<Integer> copy = new LinkedList<Integer>(lst);
	    Collections.shuffle(copy);
	    return copy.subList(0, n);
	}
}
