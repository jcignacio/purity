package purity.geneticAlgorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import net.maizegenetics.analysis.distance.IBSDistanceMatrix;
import net.maizegenetics.dna.snp.FilterGenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.TaxaListBuilder;
import net.maizegenetics.taxa.distance.DistanceMatrix;
import sun.security.util.Length;

public class GenotypeDuplicity {
    private int[] frequencyOfDuplicity;
    private int numberOfUniqueTaxa = 0;
    private int numberOfTaxaSubgroup = 0;
    private final DistanceMatrix distMatrix;
    private Double maxDistance = PassParameters.getMaxDistance();
    private ArrayList<ArrayList<Integer>> duplicatedTaxa;
    private GenotypeTable genos;
    public static enum Method {
        EXACT_MATCH, WITHIN_DISTANCE
    }
    
    public GenotypeDuplicity(GenotypeTable geno, Double maxDistance) {
        this.distMatrix = IBSDistanceMatrix.getInstance(geno);  
        this.maxDistance = maxDistance;
        Build(Method.WITHIN_DISTANCE);
    }
    
    public GenotypeDuplicity(GenotypeTable geno) {
        this.distMatrix = IBSDistanceMatrix.getInstance(geno);
        Build(Method.EXACT_MATCH);
    }
    
    public GenotypeDuplicity(DistanceMatrix distMatrix, Double maxDistance) {
        this.distMatrix = distMatrix;
        Build(Method.WITHIN_DISTANCE);
    }    

    public GenotypeDuplicity(DistanceMatrix distMatrix) {
        this.distMatrix = distMatrix;  
        Build(Method.EXACT_MATCH);
    }
    
    private void Build(Method method){
        int numberOfTaxa = this.distMatrix.getSize();
        int maxGroupSize = 0;
        boolean[] taxaToProcess = new boolean[numberOfTaxa];
        Arrays.fill(taxaToProcess, true);
        duplicatedTaxa = new ArrayList<ArrayList<Integer>>();
        LinkedList<Integer> queue = new LinkedList<Integer>();
        
        for (int i = 0; i < numberOfTaxa; i++) {
            if(taxaToProcess[i]){
                ArrayList<Integer> group = new ArrayList<Integer>();
                queue.addFirst(i);
                while (!queue.isEmpty()){
                    int taxa = queue.removeFirst();
                    if(taxaToProcess[taxa]){
                        taxaToProcess[taxa] = false;
                        group.add(taxa);
                        for (int j = taxa+1; j < numberOfTaxa; j++) {
                            switch (method){
                            case EXACT_MATCH:
                                if (distMatrix.getDistance(taxa, j)==0){
                                    queue.addFirst(j);
                                } break;
                            case WITHIN_DISTANCE:
                                if (distMatrix.getDistance(taxa, j) <= maxDistance){
                                    queue.addFirst(j);
                                } break;
                            }

                        }
                    }
                }
                    if(maxGroupSize < group.size()) maxGroupSize = group.size();
                    duplicatedTaxa.add(group);
            }
        }
        
        frequencyOfDuplicity = new int[maxGroupSize];
        numberOfTaxaSubgroup = duplicatedTaxa.size();
        
        for (int i = 0; i < duplicatedTaxa.size(); i++){
            frequencyOfDuplicity[duplicatedTaxa.get(i).size()-1]++;
        }
        
        numberOfUniqueTaxa = frequencyOfDuplicity[0];                
    }
    
    private int getSubgroupsForTaxaIndices(ArrayList<Integer> taxaList, Method method) {
        int numberOfTaxa = taxaList.size();
        int maxGroupSize = 0;
        boolean[] taxaToProcess = new boolean[numberOfTaxa];
        Arrays.fill(taxaToProcess, true);
        
        ArrayList<ArrayList<Integer>> duplicatedTaxa = new ArrayList<ArrayList<Integer>>();
        LinkedList<Integer> queue = new LinkedList<Integer>();
        
        for (int i = 0; i < numberOfTaxa; i++) {
            if(taxaToProcess[i]){
                ArrayList<Integer> group = new ArrayList<Integer>();
                queue.addFirst(i);
                while (!queue.isEmpty()){
                    int idx = queue.removeFirst();
                    int taxa = taxaList.get(idx);
                    if(taxaToProcess[idx]){
                        taxaToProcess[idx] = false;
                        group.add(taxa);
                        for (int j = idx+1; j < numberOfTaxa; j++) {
                            switch (method){
                            case EXACT_MATCH:
                                if (distMatrix.getDistance(taxaList.get(idx), taxaList.get(j)) == 0){
                                    queue.addFirst(j);
                                } break;
                            case WITHIN_DISTANCE:
                                if (distMatrix.getDistance(taxaList.get(idx), taxaList.get(j)) <= maxDistance){
                                    queue.addFirst(j);
                                } break;
                            }
                        }
                    }
                }
                    if(maxGroupSize < group.size()) maxGroupSize = group.size();
                    duplicatedTaxa.add(group);
            }
        }

        numberOfTaxaSubgroup = duplicatedTaxa.size();
        return numberOfTaxaSubgroup - 1;
    }
    
    public int getSubgroupsForListOfTaxaIndices(ArrayList<ArrayList<Integer>> taxaGroupList, Method method) {
        int totalNumberOfSubgroups = 0;
        for (int i = 0; i < taxaGroupList.size(); i++) {
        	//System.out.println("i:"+i+", size: "+taxaGroupList.get(i).size());
        	if(taxaGroupList.get(i).size() > 1) {
	        	int tmp = getSubgroupsForTaxaIndices(taxaGroupList.get(i), method);
		        totalNumberOfSubgroups += tmp;
		        //System.out.println("tmp:"+tmp);
        	}
	    }
	    
        //System.out.println(totalNumberOfSubgroups);
        return totalNumberOfSubgroups;
    }
    
    public int getUniqueTaxa(){
        return numberOfUniqueTaxa;
    }
    
    public int getTaxaSubgroup(){
        return numberOfTaxaSubgroup;
    }
    
    public int[] getFrequencyOfDuplicity(){
        return frequencyOfDuplicity;
    }
    
    public ArrayList<ArrayList<Integer>> getDuplicatedTaxa(){
        return duplicatedTaxa;
    }
         
    public void printFrequencyOfDuplicity(){
        System.out.println("Samples\tDuplicates");
        for (int i = 0; i < frequencyOfDuplicity.length; i++)
            if(frequencyOfDuplicity[i]>0)
                System.out.println((i+1)*frequencyOfDuplicity[i]+"\t"+i);
    }
    
    public void printFullDuplicityInfo(){
        printFrequencyOfDuplicity();
        System.out.println("Unique samples: "+getUniqueTaxa());
    }
    
    public void printDuplicatedTaxa(GenotypeTable geno){
        for(int i = 0; i < getDuplicatedTaxa().size(); i++){
            if(getDuplicatedTaxa().get(i).size() > 1){
                for (int j = 0; j < getDuplicatedTaxa().get(i).size(); j++)                  
                    System.out.print(geno.taxaName(getDuplicatedTaxa().get(i).get(j))+"\t");
                System.out.print("\n");
            }
        }
    }
    
    public GenotypeTable removeDuplicateTaxa(GenotypeTable geno){
        TaxaList taxa = geno.taxa();
        TaxaListBuilder tlb = new TaxaListBuilder();
        
        for(int i = 0; i < getDuplicatedTaxa().size(); i++){
            if(getDuplicatedTaxa().get(i).size() > 1){
                int indexOfTaxaToKeep = getDuplicatedTaxa().get(i).get(0);
                for (int j = 1; j < getDuplicatedTaxa().get(i).size(); j++){
                    int indexOfCurrentTaxa = getDuplicatedTaxa().get(i).get(j);
                    if(geno.totalNonMissingForTaxon(indexOfTaxaToKeep) < geno.totalNonMissingForTaxon(indexOfCurrentTaxa)){
                        indexOfTaxaToKeep = indexOfCurrentTaxa;
                    }
                }
                tlb.add(taxa.get(indexOfTaxaToKeep));
            }else{
                tlb.add(taxa.get(getDuplicatedTaxa().get(i).get(0)));
            }
        }
        
        taxa = tlb.build();
        geno = FilterGenotypeTable.getInstance(geno, taxa);
        
        return geno;
    }
 
    public static void main(String[] args) {
        String dir="C:/Users/jignacio/Google Drive/Trainings/R/purity/";
        String inFile= dir+"skim_and_3k_elite_core.hmp.txt";
        GenotypeTable genos = FilterGenotypes.sampleSitesByMAF(inFile, 0.01, 0.05);
        //GenotypeTable genos = ImportUtils.readFromHapmap(inFile);

        genos = FilterGenotypes.removeDuplicateTaxa(genos, 0.01);
        
        DistanceMatrix distMatrix = IBSDistanceMatrix.getInstance(genos);
        GenotypeDuplicity dupes = new GenotypeDuplicity(distMatrix);
        genos = FilterGenotypes.removeDuplicateTaxa(genos, 0.01);
        dupes.printFullDuplicityInfo();
    }
}

