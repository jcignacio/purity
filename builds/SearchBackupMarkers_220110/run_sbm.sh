## SearchBackupMarkers is for selecting additional informative (highly polymorphic) markers to an existing set of markers
# Written by John Carlos Ignacio
#
## Usage
# java -jar sbm.jar <Number of target markers> <Population size> <Distance bet. dup lines> <input hmp.txt> <output file> <file to backup> <opt: start pos>
#
## Parameters
# Number of target markers, N (integer): 			number of markers to select/add
# Popoulation size (integer): 					number of solutions to consider at a time (better results when higher but uses more RAM)
# Distance bet. dup lines (decimal, 0 to 1): 	genetic distance threshold for considering duplicated lines, set to 0 for exact match. Set higher for more polymorphic markers between lines.
# Input hmp.txt (file): 						hapmap file from where to pick markers from
# Output file (file): 							output file, e.g. out.txt
# File to backup (file): 						hapmap file containing existing markers (selected, validated or working markers). Must contain exact lines and their arrangement as the input hmp file.
# Start pos (integer): 							optional, skip first n markers in File to backup
#
## Results and outputs
# Score ([m1,m2,..mN --> x]): 	the score (X, shown after the arrow) denotes the number of unique genotypes
# Text file:					contains some information on the selected markers
# Hapmap file:					subset of the input hapmap file containing the selected markers only


# Select 5 additional markers to improve distinction between oryza species
java -jar sbm.jar 5 1000 0.0 files/file_to_choose_from.hmp.txt out.txt files/file_to_backup.hmp.txt

# Should result to a perfect score of 16, which is the total number of lines/species in the input file